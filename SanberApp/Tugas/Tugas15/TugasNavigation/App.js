import React, { Component } from 'react';

import Splash from './pages/Splash';
import Register from './pages/RegisterScreen';
import Login from './pages/LoginScreen';
import About from './pages/AboutScreen';
import SkillComponent from './pages/component/SkillComponents';
import ProjectComponents from './pages/component/ProjectComponents';

import AddSkill from './pages/AddSkill';
import AddSkill1 from './pages/AddSkill1';
import AddSkill2 from './pages/AddSkill2';
import AddSkill3 from './pages/AddSkill3';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tabs = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Skill = () => {
	<Stack.Navigator initialRouteName="ChooseCategory">
		<Stack.Screen name="ChooseCategory" component={AddSkill} />
		<Stack.Screen name="ChooseSkill" component={AddSkill1} />
		<Stack.Screen name="ChooseLevel" component={AddSkill2} />
		<Stack.Screen name="Final" component={AddSkill3} />
	</Stack.Navigator>;
};
const Tabss = () => (
	<Tabs.Navigator>
		<Tabs.Screen name="Skill" component={SkillComponent} />
		<Tabs.Screen name="Project" component={ProjectComponents} />
		<Tabs.Screen name="AddSkill" component={AddSkill} />
	</Tabs.Navigator>
);

const Drawing = () => (
	<Drawer.Navigator>
		<Drawer.Screen name="About" component={About} />
		<Drawer.Screen name="Tabs" component={Tabss} />
	</Drawer.Navigator>
);

class App extends Component {
	render() {
		return (
			<NavigationContainer>
				<Stack.Navigator initialRouteName="Splash">
					<Stack.Screen name="Splash" component={Splash} />
					<Stack.Screen name="Login" component={Login} />
					<Stack.Screen name="Register" component={Register} />
					<Stack.Screen name="Draw" component={Drawing} />
					<Stack.Screen name="ChooseSkill" component={AddSkill1} />
					<Stack.Screen name="ChooseLevel" component={AddSkill2} />
					<Stack.Screen name="Final" component={AddSkill3} />
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
}

export default App;
