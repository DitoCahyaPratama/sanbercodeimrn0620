import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Svg, { Path } from 'react-native-svg';

function Splash({ navigation }) {
	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<View style={styles.centerImage}>
					<Image source={require('../images/splash.png')} />
					<View style={styles.title}>
						<Text style={styles.black}>INDO</Text>
						<Text style={styles.blue}> HIRING</Text>
					</View>
				</View>
				<Svg
					height="60%"
					width="100%"
					viewBox="0 0 1440 320"
					style={{ position: 'absolute', top: 270 }}
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 1440 320"
				>
					<Path
						fill="#fff"
						fill-opacity="1"
						d="M0,224L34.3,186.7C68.6,149,137,75,206,74.7C274.3,75,343,149,411,181.3C480,213,549,203,617,176C685.7,149,754,107,823,74.7C891.4,43,960,21,1029,53.3C1097.1,85,1166,171,1234,208C1302.9,245,1371,235,1406,229.3L1440,224L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
					></Path>
				</Svg>
			</View>
			<View style={styles.body}>
				<TouchableOpacity onPress={() => navigation.navigate('Register')} style={styles.buttonsignUp}>
					<Text style={styles.signUpText}>Sign Up</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => navigation.navigate('Login')}
					style={styles.buttonsignIn}
				>
					<Text style={styles.signInText}>Sign In</Text>
				</TouchableOpacity>
				<TouchableOpacity>
					<Text style={styles.forgot}>Forgot password ?</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		justifyContent: 'space-between',
		flex: 1,
		backgroundColor: '#0779E4',
	},
	header: {
		backgroundColor: '#fff',
		height: 340,
	},
	centerImage: {
		alignItems: 'center',
		paddingTop: 60,
	},
	title: {
		paddingTop: 10,
		flexDirection: 'row',
	},
	black: {
		fontSize: 36,
		fontWeight: 'bold',
	},
	blue: {
		color: '#0779E4',
		fontSize: 36,
		fontWeight: 'bold',
	},
	body: {
		flex: 1,
		justifyContent: 'space-evenly',
		alignItems: 'center',
		paddingTop: 100,
	},

	buttonsignUp: {
		display: 'flex',
		height: 70,
		width: '80%',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#0779E4',
		borderWidth: 1,
		borderColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
	},
	signUpText: {
		fontSize: 24,
		color: '#fff',
	},
	buttonsignIn: {
		display: 'flex',
		height: 70,
		width: '80%',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
	},
	signInText: {
		fontSize: 24,
		color: '#0779E4',
	},
	forgot: {
		fontSize: 18,
		color: '#fff',
	},
});

export default Splash;
