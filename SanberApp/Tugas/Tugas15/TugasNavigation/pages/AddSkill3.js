import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import Icon from '@expo/vector-icons/MaterialIcons';
import IconSocial from '@expo/vector-icons/MaterialCommunityIcons';

function AddSkill3({ navigation }) {
	return (
		<View style={styles.container}>
			<ScrollView style={styles.body}>
				<View style={styles.header}>
					<Svg
						height="100%"
						width="100%"
						viewBox="0 0 1440 320"
						style={{ position: 'absolute', zIndex: 0, top: 0 }}
						xmlns="http://www.w3.org/2000/svg"
						viewBox="0 0 1440 320"
					>
						<Path
							fill="#0779E4"
							fill-opacity="1"
							d="M0,224L34.3,186.7C68.6,149,137,75,206,74.7C274.3,75,343,149,411,181.3C480,213,549,203,617,176C685.7,149,754,107,823,74.7C891.4,43,960,21,1029,53.3C1097.1,85,1166,171,1234,208C1302.9,245,1371,235,1406,229.3L1440,224L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
						></Path>
					</Svg>
				</View>
				<View>
					<View>
						<Text style={styles.skillTitle}>Correct This :</Text>
					</View>
					<View style={styles.choose}>
						<View style={styles.skill}>
							<View style={styles.skillCol}>
								<Text style={styles.title}>Bahasa Pemrograman</Text>
								<View style={styles.skillRow}>
									<Text style={styles.skillDesc}>Javascript</Text>
									<Text style={[styles.fontblue, styles.skillDesc]}>Intermidate</Text>
								</View>
							</View>
							<IconSocial name="react" size={80} color="#0779E4" />
						</View>
					</View>
					<View>
						<Text style={styles.skillTitle}>Percentage :</Text>
                        <View style={styles.choose}>
                                <TextInput
                                    style={styles.input}
                                    underlineColorAndroid="transparent"
                                    placeholder="Percentage"
                                    keyboardType="numeric"
                                    placeholderTextColor="#77D8D8"
                                />
                            {console.log(navigation)}
                            <TouchableOpacity style={styles.button} onPress={() => {navigation.popToTop()}}>
                                <Text style={styles.buttonText}>Finish</Text>
                            </TouchableOpacity>
                        </View>
					</View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		height: 80,
	},
	title: {
		fontSize: 25,
		fontWeight: 'bold',
	},
	blue: {
		backgroundColor: '#0779E4',
	},
	fontblue: {
		color: '#0779E4',
	},
	blue1: {
		backgroundColor: '#4CBBB9',
	},
	blue2: {
		backgroundColor: '#77D8D8',
	},
	body: {
		flex: 1,
	},
	skillTitle: { fontSize: 18, fontWeight: 'bold', padding: 20 },
	button: {
		display: 'flex',
		height: 50,
		width: '50%',
		margin: 20,
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#0779E4',
		borderWidth: 1,
		borderColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
	},
	buttonText: {
		fontSize: 18,
		color: '#fff',
	},
	choose: {
		paddingTop: 20,
		alignItems: 'center',
		justifyContent: 'center',
	},
	skill: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	skillCol: {
		flexDirection: 'column',
		justifyContent: 'center',
		margin: 20,
	},
	skillRow: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	skillDesc: {
		fontSize: 20,
    },
	input: {
		width: '100%',
		padding: 10,
		borderColor: '#77D8D8',
		borderBottomWidth: 1,
	},
});

export default AddSkill3;
