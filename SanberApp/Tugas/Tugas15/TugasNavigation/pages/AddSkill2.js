import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import Icon from '@expo/vector-icons/MaterialIcons';
import IconSocial from '@expo/vector-icons/FontAwesome';

function AddSkill2({ navigation }) {
	return (
		<View style={styles.container}>
			<ScrollView style={styles.body}>
				<View style={styles.header}>
					<Svg
						height="100%"
						width="100%"
						viewBox="0 0 1440 320"
						style={{ position: 'absolute', zIndex: 0, top: 0 }}
						xmlns="http://www.w3.org/2000/svg"
						viewBox="0 0 1440 320"
					>
						<Path
							fill="#0779E4"
							fill-opacity="1"
							d="M0,224L34.3,186.7C68.6,149,137,75,206,74.7C274.3,75,343,149,411,181.3C480,213,549,203,617,176C685.7,149,754,107,823,74.7C891.4,43,960,21,1029,53.3C1097.1,85,1166,171,1234,208C1302.9,245,1371,235,1406,229.3L1440,224L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
						></Path>
					</Svg>
				</View>
				<View>
					<View>
						<Text style={styles.skillTitle}>Choose One From This</Text>
					</View>
					<View style={styles.choose}>
                        <TouchableOpacity style={[styles.buttonSkill, styles.blue]}>
                            <Text style={styles.buttonText}>Basic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonSkill, styles.blue1]}>
                            <Text style={styles.buttonText}>Intermidate</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonSkill, styles.blue2]}>
                            <Text style={styles.buttonText}>Advance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button} onPress={() => {navigation.push('Final')}}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>
                    </View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		height: 80,
	},
	centerImage: {
		alignItems: 'center',
		paddingTop: 60,
		zIndex: 2,
	},
	title: {
		paddingTop: 10,
		flexDirection: 'row',
	},
	black: {
		fontSize: 36,
		fontWeight: 'bold',
	},
	blue: {
		backgroundColor: '#0779E4',
    },
    blue1: {
		backgroundColor: '#4CBBB9',
    },
    blue2: {
		backgroundColor: '#77D8D8',
    },
	body: {
		flex: 1,
	},
	skillTitle: { fontSize: 18, fontWeight: 'bold', padding: 20 },
    buttonSkill: {
		display: 'flex',
        height: 100,
        width: '80%',
		borderRadius: 20,
		justifyContent: 'center',
        alignItems: 'center',
        margin: 10,

		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
    },
    button:{
        display: 'flex',
        height: 50,
        width: '50%',
        margin: 20,
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#0779E4',
		borderWidth: 1,
		borderColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
    },
	buttonText: {
		fontSize: 18,
		color: '#fff',
    },
    choose: {
        paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default AddSkill2;
