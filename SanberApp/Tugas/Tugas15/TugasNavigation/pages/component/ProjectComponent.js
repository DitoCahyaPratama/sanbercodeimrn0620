import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import IconSocial from '@expo/vector-icons/FontAwesome';

function projectComponent(props) {
	return (
			<TouchableOpacity key={props.keyval} style={styles.item}>
				<IconSocial name={props.data.iconName} size={100} />
				<Text>{props.data.projectName}</Text>
				<Text style={styles.link}>{props.data.iconName == 'gitlab' ? 'Cek on Gitlab' : 'Cek on Github'}</Text>
			</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	item: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#e5e5e5',
		margin: 5,
		height: 150,
		width: 120,
	},
	link: {
		color: '#0779E4',
	},
});

export default projectComponent;
