import React from 'react';

import { View, StyleSheet, ScrollView, Text } from 'react-native';

import ProjectData from '../../data/ProjectData.json';
import ProjectComponent from './ProjectComponent';

function ProjectComponents() {
	let Project = ProjectData.projects.map((val, key) => {
		return <ProjectComponent key={key} keyval={key} data={val} />;
		// return <Text>Halo</Text>
	});
	return (
		<View style={styles.container}>
			<ScrollView style={styles.body}>
				<View style={styles.aboutSkill}>
					<View style={[styles.about2rowbetween]}>{Project}</View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	body: {
		flex: 1,
	},
	aboutSkill: {
		padding: 20,
	},
	categories: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 5,
		marginBottom: 10,
	},
	about2rowbetween: { flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap' },
});

export default ProjectComponents;
