import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';

import SkillData from '../../data/SkillData.json';
import CategoriesName from '../../data/CategoriesName.json';
import SkillComponent from './SkillComponent';
import CategoryComponent from './CategoryComponent';

function SkillScreen() {
	let Skills = SkillData.items.map((val, key) => {
		return <SkillComponent key={key} keyval={key} data={val} />;
	});
	let Categories = CategoriesName.category.map((val, key) => {
		return <CategoryComponent key={key} keyval={key} data={val} />;
	});

	return (
		<View style={styles.container}>
			<ScrollView style={styles.body}>
				<View style={styles.aboutSkill}>
					<View style={styles.categories}>{Categories}</View>
					{Skills}
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	body: {
		flex: 1,
	},
	aboutSkill: {
		padding: 20,
	},
	categories: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 5,
		marginBottom: 10,
	},
});

export default SkillScreen;
