import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import IconSocial from '@expo/vector-icons/MaterialCommunityIcons';
import Icon from '@expo/vector-icons/MaterialIcons'

export default function CategoryComponent(props) {
	return (
		<TouchableOpacity key={props.keyval} style={styles.category}>
            <Text style={styles.categoryName}>{props.data.categoryName}</Text>
        </TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	category: {
        backgroundColor: '#0779E4',
        padding: 5,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    categoryName: {
        color: '#fff',
    },
});
