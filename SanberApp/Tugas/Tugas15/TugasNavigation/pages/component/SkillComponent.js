import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import IconSocial from '@expo/vector-icons/MaterialCommunityIcons';
import Icon from '@expo/vector-icons/MaterialIcons'

export default function SkillComponent(props) {
	return (
		<View key={props.keyval} style={styles.skill}>
			<IconSocial name={props.data.iconName} size={80} color="#fff" />
            <View style={styles.skillDetail}>
                <Text style={styles.skillName}>{props.data.skillName}</Text>
                <Text style={styles.categoryName} >{props.data.categoryName}</Text>
                <View style={styles.percentCenter}>
                    <Text style={styles.percentageProgress}>{props.data.percentageProgress}</Text>
                </View>
            </View>
            <TouchableOpacity>
                <Icon name="keyboard-arrow-right" size={80} color="#fff" />
            </TouchableOpacity>
        </View>
	);
}

const styles = StyleSheet.create({
	skill: {
        backgroundColor: '#0779E4',
        borderRadius: 20,
		padding: 10,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    skillDetail: {
        flex: 1,
        flexDirection: 'column',
        paddingLeft: 20,
        justifyContent:"center",
    },
    skillName:{
        fontSize: 24,
        color: '#fff',
        fontWeight: 'bold',
    },
    categoryName: {
        color: '#fff',
    },
    percentCenter:{
        justifyContent: 'flex-end',
        alignItems:"flex-end",
    },  
    percentageProgress: {
        color: '#fff',
        fontSize: 36,
        fontWeight: 'bold',
        alignContent: 'center'
    }
});
