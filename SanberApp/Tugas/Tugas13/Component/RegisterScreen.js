import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, TextInput } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function Register({ navigation }) {
	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<View style={styles.centerImage}>
					<Image source={require('../Images/sign.png')} />
				</View>
				<Svg
					height="60%"
					width="100%"
					viewBox="0 0 1440 320"
					style={{ position: 'absolute', zIndex: 0, top: 200 }}
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 1440 320"
				>
					<Path
						fill="#0779E4"
						fill-opacity="1"
						d="M0,224L34.3,186.7C68.6,149,137,75,206,74.7C274.3,75,343,149,411,181.3C480,213,549,203,617,176C685.7,149,754,107,823,74.7C891.4,43,960,21,1029,53.3C1097.1,85,1166,171,1234,208C1302.9,245,1371,235,1406,229.3L1440,224L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
					></Path>
				</Svg>
			</View>
			<View style={styles.body}>
				<View style={styles.inputForm}>
					<View style={styles.textGroup}>
						<Text style={styles.labelInput}>Email</Text>
						<TextInput
							style={styles.input}
							underlineColorAndroid="transparent"
							placeholder="Email"
							placeholderTextColor="#77D8D8"
							autoCapitalize="none"
						/>
					</View>
					<View style={styles.textGroup}>
						<Text style={styles.labelInput}>Password</Text>
						<TextInput
							secureTextEntry={true}
							style={styles.input}
							underlineColorAndroid="transparent"
							placeholder="Password"
							placeholderTextColor="#77D8D8"
							autoCapitalize="none"
						/>
					</View>
                    <View style={styles.textGroup}>
						<Text style={styles.labelInput}>Confirm Password</Text>
						<TextInput
							secureTextEntry={true}
							style={styles.input}
							underlineColorAndroid="transparent"
							placeholder="Confirm Password"
							placeholderTextColor="#77D8D8"
							autoCapitalize="none"
						/>
					</View>
					<TouchableOpacity onPress={() => navigation.navigate('About')} style={styles.buttonsignUp}>
						<Text style={styles.signUpText}>Sign Up</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		justifyContent: 'space-between',
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		backgroundColor: '#0779E4',
		height: 240,
	},
	centerImage: {
		alignItems: 'center',
		paddingTop: 60,
		zIndex: 2,
	},
	title: {
		paddingTop: 10,
		flexDirection: 'row',
	},
	black: {
		fontSize: 36,
		fontWeight: 'bold',
	},
	blue: {
		color: '#0779E4',
		fontSize: 36,
		fontWeight: 'bold',
	},
	body: {
		flex: 1,
		justifyContent: 'space-evenly',
		alignItems: 'center',
		paddingTop: 50,
	},

	buttonsignUp: {
		display: 'flex',
		height: 70,
		width: '100%',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#0779E4',
		borderWidth: 1,
		borderColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
	},
	signUpText: {
		fontSize: 24,
		color: '#fff',
	},
	textGroup: {
		paddingBottom: 10,
	},
	labelInput: {
		textAlign: 'left',
	},
	inputForm: {
		padding: 20,
		borderRadius: 30,
		width: '80%',
        backgroundColor: '#fff',
	},
	input: {
		width: '100%',
		padding: 10,
		borderColor: '#77D8D8',
		borderBottomWidth: 1,
	},
});

export default Register;
