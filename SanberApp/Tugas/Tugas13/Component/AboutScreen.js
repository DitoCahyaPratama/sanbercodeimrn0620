import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from '@expo/vector-icons/MaterialIcons';
import IconSocial from '@expo/vector-icons/FontAwesome';

function Login({ navigation }) {
	return (
		<View style={styles.container}>
			<ScrollView style={styles.body}>
				<View style={styles.header}>
					<Svg
						height="100%"
						width="100%"
						viewBox="0 0 1440 320"
						style={{ position: 'absolute', zIndex: 0, top: 0 }}
						xmlns="http://www.w3.org/2000/svg"
						viewBox="0 0 1440 320"
					>
						<Path
							fill="#0779E4"
							fill-opacity="1"
							d="M0,224L34.3,186.7C68.6,149,137,75,206,74.7C274.3,75,343,149,411,181.3C480,213,549,203,617,176C685.7,149,754,107,823,74.7C891.4,43,960,21,1029,53.3C1097.1,85,1166,171,1234,208C1302.9,245,1371,235,1406,229.3L1440,224L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
						></Path>
					</Svg>
				</View>
				<View>
					<View style={styles.AboutCenter}>
						<TouchableOpacity style={styles.profile}></TouchableOpacity>
						<Text style={{ fontSize: 18, paddingTop: 10 }}>Dito Cahya Pratama</Text>
						<View style={styles.address}>
							<Icon name="location-on" size={20} color="#999"></Icon>
							<Text>Kepanjen, Malang</Text>
						</View>
					</View>
					<View style={styles.AboutMe}>
						<Text style={styles.aboutTitle}>About Me</Text>
						<Text style={styles.aboutDesc}>
							Iam fullstack developer, i have advantages can work in punishment. As you know i can typing
							fast{' '}
						</Text>
					</View>
					<View style={styles.SocialMedia}>
						<Text style={styles.aboutTitle}>My Social Media</Text>
						<View style={styles.SocialBar}>
							<TouchableOpacity style={styles.tabItem}>
								<IconSocial name="facebook" size={25} color="#0779E4" />
								<Text style={styles.tabTitle}>Facebook</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.tabItem}>
								<IconSocial name="instagram" size={25} color="#999999" />
								<Text style={styles.tabTitle}>Instagram</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.tabItem}>
								<IconSocial name="twitter" size={25} color="#999999" />
								<Text style={styles.tabTitle}>Twitter</Text>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.AboutMe}>
						<View style={styles.about2row}>
							<TouchableOpacity>
								<Text style={styles.aboutTitle}>My Portofolio</Text>
							</TouchableOpacity>
							<TouchableOpacity>
								<Text style={styles.aboutTitle}>My Skill</Text>
							</TouchableOpacity>
						</View>
						<View style={[styles.about2rowbetween]}>
							<TouchableOpacity style={styles.item}>
								<IconSocial name="github" size={100} color="#0c0c0c" />
								<Text>Hospital System</Text>
								<Text style={styles.link}>Cek on Github</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.item}>
								<IconSocial name="gitlab" size={100} color="#E24329" />
								<Text>School System</Text>
								<Text style={styles.link}>Cek on Gitlab</Text>
							</TouchableOpacity>
						</View>
						<View style={[styles.about2rowbetween]}>
							<TouchableOpacity style={styles.item}>
								<IconSocial name="github" size={100} color="#0c0c0c" />
								<Text>Video player</Text>
								<Text style={styles.link}>Cek on Github</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.item}>
								<IconSocial name="gitlab" size={100} color="#E24329" />
								<Text>Music Player</Text>
								<Text style={styles.link}>Cek on Gitlab</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</ScrollView>
			<View style={styles.bottom}>
				<TouchableOpacity style={styles.button}>
					<Text style={styles.buttonText}>+ Skill</Text>
				</TouchableOpacity>
                <TouchableOpacity style={styles.button}>
					<Text style={styles.buttonText}>+ Portofolio</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		height: 80,
	},
	centerImage: {
		alignItems: 'center',
		paddingTop: 60,
		zIndex: 2,
	},
	title: {
		paddingTop: 10,
		flexDirection: 'row',
	},
	black: {
		fontSize: 36,
		fontWeight: 'bold',
	},
	blue: {
		color: '#0779E4',
		fontSize: 36,
		fontWeight: 'bold',
	},
	link: {
		color: '#0779E4',
	},
	body: {
		flex: 1,
	},
	profile: {
		width: 100,
		height: 100,
		backgroundColor: '#c4c4c4',
		borderRadius: 50,
	},
	AboutCenter: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	address: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	aboutTitle: { fontSize: 18, fontWeight: 'bold', padding: 20 },
	aboutDesc: {
		padding: 20,
	},
	SocialBar: {
		backgroundColor: '#fafafa',
		height: 80,
		borderColor: '#E5E5E5',
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	tabItem: {
		alignItems: 'center',
		justifyContent: 'center',
	},
	tabTitle: {
		fontSize: 11,
		color: '#3c3c3c',
		paddingTop: 3,
	},
	about2row: { flexDirection: 'row', justifyContent: 'center' },
	about2rowbetween: { flexDirection: 'row', justifyContent: 'space-around' },
	item: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#e5e5e5',
		margin: 20,
		height: 150,
		width: 150,
	},
	bottom: {
		backgroundColor: '#fafafa',
		height: 80,
		borderColor: '#E5E5E5',
        flexDirection: 'row',
        alignItems: 'center',
		justifyContent: 'space-around',
    },
    button: {
		display: 'flex',
        height: 50,
        width: '50%',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',

		backgroundColor: '#0779E4',
		borderWidth: 1,
		borderColor: '#fff',
		shadowOpacity: 0.4,
		shadowOffset: { height: 10, width: 0 },
		shadowRadius: 20,
	},
	buttonText: {
		fontSize: 18,
		color: '#fff',
	},
});

export default Login;
