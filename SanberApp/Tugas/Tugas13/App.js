import React, { Component } from 'react';
import Splash from './Component/Splash'
import Register from './Component/RegisterScreen'
import Login from './Component/LoginScreen'
import About from './Component/AboutScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class App extends Component {
	render() {
		const Stack = createStackNavigator();
		return (
			<NavigationContainer>
      			<Stack.Navigator initialRouteName="Splash">
					<Stack.Screen name="Splash" component={Splash} />
					<Stack.Screen name="Register" component={Register} />
					<Stack.Screen name="Login" component={Login} />
					<Stack.Screen name="About" component={About} />
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
}

export default App;
