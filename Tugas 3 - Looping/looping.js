//Tugas 1
let no = 1;
while (no > 0) {
    if (no == 1) {
        console.log('LOOPING PERTAMA');
        no++;
    } else if (no == 20) {
        no++;
        break;
    } else {
        console.log(no + "- I love coding");
        no += 2;
    }
}
while (no <= 21) {
    if (no == 21) {
        console.log('LOOPING KEDUA');
        no--;
    } else if (no < 0) {
        break;
    } else {
        console.log(no + "- I will become a mobile developer");
        no -= 2;
    }
}

console.log();

// Tugas 2

for (let angka = 1; angka <= 20; angka++) {
    if (angka % 3 == 0 && angka % 2 == 1) {
        console.log(angka + " - I Love Coding")
    }else if (angka % 2 == 1) {
        console.log(angka + " - Santai")
    } else if (angka % 2 == 0) {
        console.log(angka + " - Berkualitas")
    }
}

console.log();

//Tugas 3

for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 8; j++) {
        process.stdout.write("#");
    }
    console.log();
}

// Tugas 4
for (let i = 0; i < 8; i++) {
    let pagar = "#".repeat(i);
    console.log(pagar);
}

console.log();
// atau bisa dengan menggunakan nested looping

for (let i = 1; i < 8; i++) {
    for (let j = 0; j < i; j++) {
        process.stdout.write("#");
    }
    console.log();
}

console.log();
// Tugas 5

for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
        if(i % 2 == 0){
            if(j % 2 == 0){
                process.stdout.write(" ");
            }else{
                process.stdout.write("#");
            }
        }else{
            if(j % 2 == 0){
                process.stdout.write("#");
            }else{
                process.stdout.write(" ");
            }
        }
    }
    console.log();
}