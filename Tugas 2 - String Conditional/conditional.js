//Tugas if else

var nama = "junaedi"
var peran = "werewolf"

if (nama.length != 0 && peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if (nama.toLowerCase() == "jane" && peran.toLowerCase() == "penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama.toLowerCase() == "jenita" && peran.toLowerCase() == "guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama.toLowerCase() == "junaedi" && peran.toLowerCase() == "werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
} else {
    console.log("Nama harus diisi!");
}

//Tugas Switch Case

var hari = 21;
var bulan = 1;
var tahun = 1945;

var bulanNama;

if (hari >= 1 && hari <= 31) {
    if (bulan >= 1 && bulan <= 12) {
        if (tahun >= 1900 && tahun <= 2200) {
            switch (bulan) {
                case 1:
                    bulanNama = "Januari";
                    break;
                case 2:
                    bulanNama = "Februari";
                    break;
                case 3:
                    bulanNama = "Maret";
                    break;
                case 4:
                    bulanNama = "April";
                    break;
                case 5:
                    bulanNama = "Mei";
                    break;
                case 6:
                    bulanNama = "Juni";
                    break;
                case 7:
                    bulanNama = "Juli";
                    break;
                case 8:
                    bulanNama = "Agustus";
                    break;
                case 9:
                    bulanNama = "September";
                    break;
                case 10:
                    bulanNama = "Oktober";
                    break;
                case 11:
                    bulanNama = "November";
                    break;
                case 12:
                    bulanNama = "Desember";
                    break;
                default:
                    break;
            }
            console.log(hari +" "+ bulanNama +" "+ tahun);
        } else {
            console.log('Masukkan tahun diantara (1900-2200)');
        }
    } else {
        console.log('Masukkan bulan dengan benar (1-12)');
    }
} else {
    console.log('Masukkan tanggal dengan benar (1-31)');
}