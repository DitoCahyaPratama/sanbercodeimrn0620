// Soal no 1

function range(startNum, finishNum) {
    if (startNum == '') {
        return -1
    } else if (finishNum < startNum) {
        let array = [];
        for (let i = startNum; i >= finishNum; i--) {
            array.push(i);
        }
        return array;
    } else if (startNum < finishNum) {
        let array = [];
        for (let i = startNum; i <= finishNum; i++) {
            array.push(i);
        }
        return array;
    } else {
        return -1
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log();
// Soal no 2

function rangeWithStep(startNum, finishNum, step) {
    if (startNum == '') {
        return -1
    } else if (finishNum < startNum) {
        let array = [];
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i);
        }
        return array;
    } else if (startNum < finishNum) {
        let array = [];
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i);
        }
        return array;
    } else {
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log();
// Soal no 3

function sum(startNum = '', finishNum = '', step = '') {
    if (startNum == '') {
        return 0;
    } else if (startNum != '' && finishNum == '' && step == '') {
        return startNum;
    } else if (startNum != '' && finishNum != '' && step == '') {
        let nilai = 0;
        const array = range(startNum, finishNum)
        for (let i = 0; i < array.length; i++) {
            nilai += array[i];
        }
        return nilai;
    } else if (startNum != '' && finishNum != '' && step != '') {
        let nilai = 0;
        const array = rangeWithStep(startNum, finishNum, step)
        for (let i = 0; i < array.length; i++) {
            nilai += array[i];
        }
        return nilai;
    }
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log();
// Soal 4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]


function dataHandling(data) {
    var hasil = '';
    for (let i = 0; i < data.length; i++) {
        hasil += `Nomor ID: ${input[i][0]} \n`;
        hasil += `Nama Lengkap: ${input[i][1]} \n`;
        hasil += `TTL: ${input[i][2]} ${input[i][3]}\n`;
        hasil += `Hobi: ${input[i][4]} \n\n`;
    }
    return hasil;
}

console.log(dataHandling(input));


console.log();
// Soal 5

function balikKata(word) {
    let kata = '';
    for (let i = word.length - 1; i > -1; i--) {
        kata += word[i]
    }
    return kata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers"))


console.log();
// Soal 6

const input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(data) {
    let dt = data;
    dt.splice(1, 1, "Roman Alamsyah Elsharawy");
    dt.splice(2, 1, "Provinsi Bandar Lampung");
    dt.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(dt)
    let tanggal = dt[3].split("/");
    let tanggalBaru = [];
    for (let i = 0; i < tanggal.length; i++) {
        tanggalBaru.push(parseInt(tanggal[i]))
    }
    let bulanAngka = tanggalBaru[1];
    const bl = bulan(bulanAngka)
    console.log(bl);
    let tanggalSort = tanggalBaru.sort(function (value1, value2) { return value1 < value2 } )
    console.log(tanggalSort)
    let tanggalJoin = tanggal.join("-")
    console.log(tanggalJoin)
    let nama = dt[1].slice(0,15)
    console.log(nama)
}

function bulan(bln) {
    switch (bln) {
        case 1:
            return "Januari";
        case 2:
            return "Februari";
        case 3:
            return "Maret";
        case 4:
            return "April";
        case 5:
            return "Mei";
        case 6:
            return "Juni";
        case 7:
            return "Juli";
        case 8:
            return "Agustus";
        case 9:
            return "September";
        case 10:
            return "Oktober";
        case 11:
            return "November";
        case 12:
            return "Desember";
        default:
            return "Bulan Salah";
            break;
    }
}

dataHandling2(input2)