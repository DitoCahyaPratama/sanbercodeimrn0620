// Soal 1

// Bisa menggunakan function ini

// function arrayToObject(arr) {
//     // Code di sini
//     let object = {}
//     const now = new Date()
//     const thisYear = now.getFullYear()
//     let age = 0
//     arr.forEach(function (item, index) {
//         age = (item[3] != null ? thisYear - item[3] :
//             "Invalid Birth Year")

//         if (age < 0){
//             age = "Invalid Birth Year"
//         }

//         object[`${index+1}. ${item[0]} ${item[1]}`] = {
//             "firstname": item[0],
//             "lastname": item[1],
//             "gender": item[2],
//             "age": age
//         }
//     })
//     console.log(object)
// }

// Atau juga bisa menampilkan data dengan menggunakan log

function arrayToObject(arr) {
    // Code di sini
    let object = {}
    const now = new Date()
    const thisYear = now.getFullYear()
    let age = 0
    arr.forEach(function (item, index) {
        age = (item[3] != null ? thisYear - item[3] :
            "Invalid Birth Year")

        if (age < 0){
            age = "Invalid Birth Year"
        }

        object[index] = {
            "firstname": item[0],
            "lastname": item[1],
            "gender": item[2],
            "age": age
        }

        console.log(`${index + 1}. ${object[`${index}`].firstname} ${object[`${index}`].lastname} : {
            firstname: "${object[`${index}`].firstname}"
            lastname: "${object[`${index}`].lastname}"
            gender: "${object[`${index}`].gender}"
            age: "${object[`${index}`].age}"
        }`)
    })
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


console.log();
console.log();
console.log();

// Soal 2

function shoppingTime(memberId, money) {
    // you can only write your code here!
    let shopping = {}
    let data = []
    let wallet = money;
    if (memberId == "" || money == null) {
        shopping["error"] = "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        shopping["error"] = "Mohon maaf, uang tidak cukup"
    } else {
        if (money >= 1500000) {
            data.push('Sepatu Brand Stacattu')
            wallet -= 1500000
        }
        if (money >= 500000) {
            data.push('Baju Zoro')
            wallet -= 500000
        }
        if (money >= 250000) {
            data.push('Baju brand H&N')
            wallet -= 250000
        }
        if (money >= 175000) {
            data.push('Sweater Uniklooh')
            wallet -= 175000
        }
        if (money >= 50000) {
            data.push('Casing Handphone')
            wallet -= 50000
        }
        shopping.memberId = memberId
        shopping.money = money
        shopping.listPurchased = data
        shopping.changeMoney = wallet
    }
    return shopping
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log();
console.log();
console.log();

// Soal 3

function naikAngkot(arrPenumpang) {
    const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let array = []
    
    arrPenumpang.forEach(function(arr, index){
        let object = {}
        let from = 0, from1 = 0
        let to = 0, to1 = 0
        let pay = 0;
        rute.forEach(function(arr2, index2){
            if(arr[1] == arr2){
                from = index2
                from1 = index2
            }
            if(arr[2] == arr2){
                to = index2
                to1 = index2
            }
            if(from < to){
                from = from1
                to = to1
            }else{
                from = to1
                to = from1
            }
            for (let i = from; i < to; i++) {
                pay +=  2000
            }
            object.penumpang = arr[0]
            object.naikDari = arr[1]
            object.tujuan = arr[2]
            object.bayar = pay
            pay = 0;
        })
        array.push(object)
    }) 
    return array
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]