var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
let waktu = 10000;

const baca = (time, n) => {
  readBooksPromise(time, books[n])
    .then((fulfilled) => {
      if (n != books.length - 1 && fulfilled >= books[n + 1].timeSpent) {
        baca(fulfilled, n + 1);
      }else{
        baca(fulfilled, n + 1);
      }
    })
    .catch((error) => {
    //   console.log(error);
    });
};

baca(waktu, 0);
