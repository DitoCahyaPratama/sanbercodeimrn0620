var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini
let waktu = 10000;

var baca = function (time, n) {
  readBooks(time, books[n], function (check) {
    if (n != books.length - 1 && check >= books[n+1].timeSpent) {
      baca(check, n+1)
    }
  });
};

baca(waktu, 0);
