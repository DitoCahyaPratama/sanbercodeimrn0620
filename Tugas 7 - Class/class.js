// Soal 1

class Animal {
    // Code class di sini
    constructor(name){
        this.name = name
        this.leg = 4
        this.blood = false
    }
    get legs(){
        return this.leg;
    }
    get cold_blooded(){
        return this.blood
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


// Soal 2

// Code class Ape dan class Frog di sini

class Ape extends Animal{
    constructor(name){
        super()
        this.name = name
        this.leg = 2
    }
    yell(){
        return console.log("Auooo");
    }
}

class Frog extends Animal{
    constructor(name){
        super()
        this.name = name
    }
    jump(){
        return console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
// console.log(sungokong.legs)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
// console.log(kodok.legs); 


// // Soal 3

// function Clock({
//     template
// }) {

//     var timer;

//     function render() {
//         var date = new Date();

//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;

//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;

//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;

//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);

//         console.log(output);
//     }

//     this.stop = function () {
//         clearInterval(timer);
//     };

//     this.start = function () {
//         render();
//         timer = setInterval(render, 1000);
//     };

// }

// var clock = new Clock({
//     template: 'h:m:s'
// });
// clock.start();

class Clock {
    // Code di sini
    constructor({template}){
        this.template = template
        this.timer
    }
    render(){
        const date = new Date()

        const hours = date.getHours()
        if(hours < 10) hours = '0' + hours

        const mins = date.getMinutes()
        if(mins < 10) mins = '0' + mins

        const secs = date.getSeconds()
        if(secs < 10) secs = '0' + secs

        const output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)
    
        console.log(output)
    }
    stop(){
        clearInterval(this.timer)
    }
    start(){
        this.render()
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();