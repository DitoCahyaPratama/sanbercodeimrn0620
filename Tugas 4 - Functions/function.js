// Tugas 1

/*
    Tulis code function di sini
*/
function teriak(){
    return 'Halo Sanbers!';
}
 
console.log(teriak()) // "Halo Sanbers!" 



console.log();

//Tugas 2

/*
    Tulis code function di sini
*/

function multiply(num1, num2){
    return num1 * num2;
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = multiply(num1, num2)
console.log(hasilKali) // 30 



console.log();

// Tugas 3

/* 
    Tulis kode function di sini
*/
function introduce(nm, age, adrs, hb){
    return `Nama saya ${nm}, umur saya ${age} tahun, alamat saya di ${adrs}, dan saya punya hobby yaitu ${hb}`;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 